export type TInputValue = number | string | null;
export type TInputType = 'checkbox' | 'text' | 'number' | 'radio' | 'password';
export interface IColor {
  name?: string;
}
export interface IColor {
  red: number;
  green: number;
  blue: number;
}
